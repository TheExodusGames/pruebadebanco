using System;

namespace Simulaciondebanco.
{
    class Program
    {
        static void Main(string[] args)
        {
            Personas persona = new Personas();
            int deposito = 0;
            int depositos = 0;
            int retiro = 0;
            int numero;
            int opcion;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Bienvenidos al banco de 3 clientes");
            do{
            Console.WriteLine("Para depositar o retirar dinero, primero identifiquese.");
            Console.WriteLine("Digite su ID personal.");
            Console.WriteLine("1. Alexander Coronado");
            Console.WriteLine("2. Coronado Alexander.");
            Console.WriteLine("3. Alesito Coronadito");
            Console.WriteLine("4. Salir");
            numero = int.Parse(Console.ReadLine());

            switch(numero){
                case 1: 
                        Console.WriteLine("---------------------------------------");
                        persona.mostrarpersona1();
                        Console.WriteLine("---------------------------------------");
                        Console.WriteLine("Digite el numero de la accion que quiere hacer.");
                        Console.WriteLine("1. Depositar");
                        Console.WriteLine("2. Retirar");
                        Console.WriteLine("---------------------------------------");
                        opcion = int.Parse(Console.ReadLine());
                        switch(opcion){
                            case 1: 
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Cuanto dinero quiere depositar?");
                                Console.WriteLine("---------------------------------------");
                                deposito = int.Parse(Console.ReadLine());
                                persona.p1m = persona.p1m + deposito;
                                Console.WriteLine(persona.p1m);
                            break;
                            case 2: 
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Cuanto dinero quiere retirar?");
                                Console.WriteLine("---------------------------------------");
                                retiro = int.Parse(Console.ReadLine());
                                persona.p1m = persona.p1m - retiro;
                                Console.WriteLine(persona.p1m);
                            break;
                            default:
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Opcion desconocida.");
                                Console.WriteLine("---------------------------------------");
                            break;
                        };
                break;
                case 2: 
                        Console.WriteLine("---------------------------------------");
                        persona.mostrarpersona2();
                        Console.WriteLine("---------------------------------------");
                        Console.WriteLine("Digite el numero de la accion que quiere hacer.");
                        Console.WriteLine("1. Depositar");
                        Console.WriteLine("2. Retirar");
                        Console.WriteLine("---------------------------------------");
                        opcion = int.Parse(Console.ReadLine());
                        switch(opcion){
                            case 1: 
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Cuanto dinero quiere depositar?");
                                Console.WriteLine("---------------------------------------");
                                deposito = int.Parse(Console.ReadLine());
                                persona.p2m = persona.p2m + deposito;
                                Console.WriteLine(persona.p2m);
                            break;
                            case 2: 
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Cuanto dinero quiere retirar?");
                                Console.WriteLine("---------------------------------------");
                                retiro = int.Parse(Console.ReadLine());
                                persona.p2m = persona.p2m - retiro;
                                Console.WriteLine(persona.p2m);
                            break;
                            default:
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Opcion desconocida.");
                                Console.WriteLine("---------------------------------------");
                            break;
                        };
                break;
                case 3:
                        Console.WriteLine("---------------------------------------");
                        persona.mostrarpersona3();
                        Console.WriteLine("---------------------------------------");
                        Console.WriteLine("Digite el numero de la accion que quiere hacer.");
                        Console.WriteLine("1. Depositar");
                        Console.WriteLine("2. Retirar");
                        Console.WriteLine("---------------------------------------");
                        opcion = int.Parse(Console.ReadLine());
                        switch(opcion){
                            case 1: 
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Cuanto dinero quiere depositar?");
                                Console.WriteLine("---------------------------------------");
                                deposito = int.Parse(Console.ReadLine());
                                persona.p3m = persona.p3m + deposito;
                                Console.WriteLine(persona.p3m);
                            break;
                            case 2: 
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Cuanto dinero quiere retirar?");
                                Console.WriteLine("---------------------------------------");
                                retiro = int.Parse(Console.ReadLine());
                                persona.p3m = persona.p3m - retiro;
                                Console.WriteLine(persona.p3m);
                            break;
                            default:
                                Console.WriteLine("---------------------------------------");
                                Console.WriteLine("Opcion desconocida.");
                                Console.WriteLine("---------------------------------------");
                            break;
                        };
                break;
                case 4: 
                        Console.WriteLine("---------------------------------------");
                        Console.WriteLine("Gracias por usar nuestro sistema.");
                        Console.WriteLine("---------------------------------------");
                        depositos = persona.p1m + persona.p2m + persona.p3m;
                        Console.WriteLine("El total del dinero depositado es: " + depositos);
                        Console.WriteLine("---------------------------------------");
                        Console.WriteLine("CREADO TOTALMENTE POR ALEXANDER.");
                        Console.WriteLine("---------------------------------------");
                break;
            }
            }while (numero!=4);
        }
    }

    public class Personas
    {
            string p1n = "Alex";
            string p1a = "Coronado";
            string p1c = "047-123456-7";
            string p1t = "809-618-3879";
            string p1na = "Dominicano";
            public int p1m;

            string p2n = "Coronado";
            string p2a = "Alexander";
            string p2c = "047-654321-7";
            string p2t = "809-618-7938";
            string p2na = "Dominicano";
            public int p2m;
            
            string p3n = "Alesito";
            string p3a = "Coronadito";
            string p3c = "047-132465-7";
            string p3t = "809-618-9783";
            string p3na = "Dominicano";
            public int p3m;
        
        public void mostrarpersona1()
        {
            Console.WriteLine("Nombre: " + p1n);
            Console.WriteLine("Apellido: " + p1a);
            Console.WriteLine("Cedula: " + p1c);
            Console.WriteLine("Telefono: " + p1t);
            Console.WriteLine("Nacionalidad: " + p1na);
            Console.WriteLine("Deposito: " + p1m);
        }
         public void mostrarpersona2()
        {
            Console.WriteLine("Nombre: " + p2n);
            Console.WriteLine("Apellido: " + p2a);
            Console.WriteLine("Cedula: " + p2c);
            Console.WriteLine("Telefono: " + p2t);
            Console.WriteLine("Nacionalidad: " + p2na);
            Console.WriteLine("Deposito: " + p2m);

        }
        public void mostrarpersona3()
        {
            Console.WriteLine("Nombre: " + p3n);
            Console.WriteLine("Apellido: " + p3a);
            Console.WriteLine("Cedula: " + p3c);
            Console.WriteLine("Telefono: " + p3t);
            Console.WriteLine("Nacionalidad: " + p3na);
            Console.WriteLine("Deposito: " + p3m);

        }


    }
}
